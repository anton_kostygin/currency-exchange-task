export const GET_ACCOUNTS = 'accounts.json';
export const GET_CURRENCIES = 'api/currencies.json';
export const GET_EXCHANGE_RATES = 'api/latest.json';
