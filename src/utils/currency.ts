import { ExchangeRate } from 'types/exchangeRate';

export function convertAmount(amount: number, rate: number) {
  return Math.round(100 * amount * rate) / 100;
}

export function convertCurrencyRates(
  targetCurrency: string,
  { base, rates }: ExchangeRate,
): ExchangeRate {
  const baseRates = { ...rates, [base]: 1 };
  const baseTargetRatio = baseRates[targetCurrency];
  if (!baseTargetRatio) {
    throw new Error("Can't convert currency: not enough information about rates");
  }
  const targetBaseRatio = 1 / baseTargetRatio;
  const targetRates = Object.entries(baseRates)
    .filter(([cur]) => cur !== targetCurrency)
    .reduce<Record<string, number>>((acc, [currencyCode, rate]) => {
    acc[currencyCode] = rate * targetBaseRatio;
    return acc;
  }, {});

  return {
    base: targetCurrency,
    rates: targetRates,
  };
}

export function formatAmount(amount: number) {
  return new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
  }).format(amount).slice(1);
}

export function formatCurrency(amount: number, currencyCode: string) {
  return new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: currencyCode,
  }).format(amount);
}
