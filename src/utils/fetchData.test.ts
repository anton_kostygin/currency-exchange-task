import fetchData from './fetchData';

describe('fetchData', () => {
  it('success response', async () => {
    global.fetch = jest.fn().mockImplementationOnce(() => Promise.resolve({
      ok: true,
      json: () => Promise.resolve({ test: 'test' }),
    }));

    const data = await fetchData('/test', { key: 'value' });
    expect(global.fetch).toBeCalledWith(expect.stringMatching(/test/));
    expect(global.fetch).toBeCalledWith(expect.stringMatching('key=value'));
    expect(data).toEqual({ test: 'test' });
    expect(global.fetch).toBeCalledTimes(1);
  });

  it('failure response', async () => {
    global.fetch = jest.fn().mockImplementationOnce(() => Promise.resolve({
      ok: false,
      json: () => Promise.reject(new Error('Some error')),
    }));

    await expect(fetchData('/test', { key: 'value' })).rejects.toThrow();
  });
});
