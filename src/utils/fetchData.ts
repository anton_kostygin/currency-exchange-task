export default async function fetchData(url: string, options?: Record<string, string | number>) {
  const params = new URLSearchParams();
  params.set('app_id', process.env.REACT_APP_API_KEY || '');
  params.set('prettyprint', 'false');
  Object.entries(options || {}).forEach(([key, value]) => {
    params.set(key, value.toString());
  });

  const response = await fetch(`${url}?${params.toString()}`);

  if (!response.ok) {
    throw new Error('Network response was not ok');
  }

  return await response.json();
}
