import { ExchangeRate } from '../types/exchangeRate';
import {
  convertAmount, convertCurrencyRates, formatAmount, formatCurrency,
} from './currency';

describe('currency', () => {
  describe('convertAmount', () => {
    it('rates', () => {
      expect(convertAmount(100, 1.1)).toBe(110);
      expect(convertAmount(100, 0.7)).toBe(70);
    });
    it('zero rate', () => {
      expect(convertAmount(100, 0)).toBe(0);
    });
    it('zero amount', () => {
      expect(convertAmount(0, 0.7)).toBe(0);
    });
  });

  describe('convertCurrencyRates', () => {
    const TEST_RATES: ExchangeRate = {
      base: 'EUR',
      rates: {
        USD: 0.8,
        GBP: 1.2,
      },
    };

    it('success convert', () => {
      const result = convertCurrencyRates('USD', TEST_RATES);
      expect(result.base).toBe('USD');
      expect(result.rates).toEqual({
        EUR: 1.25,
        GBP: 1.5,
      });
    });

    it('not enough information about rates', () => {
      expect(() => convertCurrencyRates('XXX', TEST_RATES)).toThrow();
    });
  });

  describe('formatAmount', () => {
    it('zero', () => {
      expect(formatAmount(0)).toBe('0.00');
    });
    it('thousands', () => {
      expect(formatAmount(5234.56)).toBe('5,234.56');
    });
    it('millions', () => {
      expect(formatAmount(1010101.3333)).toBe('1,010,101.33');
    });
    it('round amount', () => {
      expect(formatAmount(0.035)).toBe('0.04');
      expect(formatAmount(0.034)).toBe('0.03');
    });
  });

  describe('formatCurrency', () => {
    it('zero', () => {
      expect(formatCurrency(0, 'USD')).toBe('$0.00');
    });
    it('round amount', () => {
      expect(formatCurrency(1.356, 'GBP')).toBe('£1.36');
      expect(formatCurrency(1.351, 'GBP')).toBe('£1.35');
    });
    it('thousands', () => {
      expect(formatCurrency(5555.32, 'EUR')).toBe('€5,555.32');
    });
    it('millions', () => {
      expect(formatCurrency(6666666.11, 'USD')).toBe('$6,666,666.11');
    });
    it('unknown currency', () => {
      expect(formatCurrency(100000, 'DDD')).toBe('DDD 100,000.00');
    });
  });
});
