export type ExchangeRate = {
  base: string,
  rates: Record<string, number>
};
