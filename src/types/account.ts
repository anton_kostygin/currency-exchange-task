export type AccountType = {
  amount: number,
  currencyCode: string,
  currencyName?: string,
  isExchangeable: boolean,
};
