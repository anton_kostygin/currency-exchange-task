import React from 'react';
import useAccounts from 'hooks/useAccounts';
import CurrencyExchanger from 'components/CurrencyExchanger';
import styles from './App.module.css';

export default function App() {
  const { isLoading } = useAccounts();

  return (
    <main className={styles.layout}>
      {
          isLoading
            ? <div>Loading...</div>
            : <CurrencyExchanger />
        }
    </main>
  );
}
