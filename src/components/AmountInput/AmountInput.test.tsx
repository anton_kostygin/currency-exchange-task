import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import AmountInput from './AmountInput';
import styles from './AmountInput.module.css';

describe('AmountInput', () => {
  it('render', async () => {
    const {
      getByDisplayValue,
    } = render(<AmountInput value={100} />);

    expect(getByDisplayValue('100.00')).toBeInTheDocument();
    expect(getByDisplayValue('100.00')).toHaveClass(styles.input);
  });

  it('value', async () => {
    const { getByDisplayValue, rerender } = render(
      <AmountInput value={500} />,
    );

    expect(() => getByDisplayValue('500.00')).not.toThrow();
    expect(() => getByDisplayValue('300.00')).toThrow();

    rerender(<AmountInput value={300} />);

    expect(() => getByDisplayValue('300.00')).not.toThrow();
    expect(() => getByDisplayValue('500.00')).toThrow();
  });

  it('onChange', () => {
    const mockOnChange = jest.fn();

    const { getByDisplayValue } = render(
      <AmountInput value={1} onChange={mockOnChange} />,
    );

    fireEvent.change(getByDisplayValue('1.00'), {
      target: { value: 2 },
    });

    expect(mockOnChange).toBeCalledTimes(1);
    expect(mockOnChange.mock.calls[0][0]).toBe(2);
  });

  it('onFocus', () => {
    const mockOnFocus = jest.fn();

    const { getByDisplayValue } = render(
      <AmountInput value={1} onFocus={mockOnFocus} />,
    );

    fireEvent.focus(getByDisplayValue('1.00'));

    expect(mockOnFocus).toBeCalledTimes(1);

    expect(getByDisplayValue('1')).toBeInTheDocument();
    expect(() => getByDisplayValue('1.00')).toThrow();
  });

  it('max', () => {
    const mockOnChange = jest.fn();

    const { getByDisplayValue } = render(
      <AmountInput value={1} max={5} onChange={mockOnChange} />,
    );

    fireEvent.change(getByDisplayValue('1.00'), {
      target: { value: 2 },
    });

    expect(mockOnChange).toBeCalledTimes(1);
    expect(mockOnChange.mock.calls[0][0]).toBe(2);

    fireEvent.change(getByDisplayValue('1.00'), {
      target: { value: 7 },
    });

    expect(mockOnChange).toBeCalledTimes(1);
  });
});
