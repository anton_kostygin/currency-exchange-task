import { FocusEventHandler } from 'react';

export type AmountInputPropsType = {
  onChange?(value: number): void;
  onFocus?: FocusEventHandler<HTMLInputElement>;
  value?: number;
  max?: number
};
