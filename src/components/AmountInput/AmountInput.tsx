import React, {
  useCallback, useEffect, useState,
} from 'react';
import { formatAmount } from 'utils/currency';
import { AmountInputPropsType } from './AmountInput.types';
import styles from './AmountInput.module.css';

const AmountInput = React.forwardRef<HTMLInputElement, AmountInputPropsType>(({
  value,
  max,
  onChange,
  onFocus,
}, ref) => {
  const [isFocused, setFocused] = useState(false);
  const [rawValue, setRawValue] = useState<string>(value ? value.toString() : '');
  const [formattedValue, setFormattedValue] = useState<string>('');

  useEffect(() => {
    setFormattedValue(
      value !== 0 ? formatAmount(value || 0) : '',
    );
  }, [value]);

  useEffect(() => {
    if (value !== parseFloat(rawValue)) {
      setRawValue(value ? value.toString() : '');
    }
  }, [value, rawValue]);

  const handleOnFocus = useCallback(
    (e) => {
      setRawValue(value ? value.toString() : '');
      setFocused(true);
      onFocus?.(e);
    },
    [onFocus, value],
  );

  const handleOnBlur = useCallback(
    () => setFocused(false),
    [],
  );

  const handleOnChange = useCallback(
    ({
      target: { value: targetValue },
    }: React.ChangeEvent<HTMLInputElement>) => {
      const newRawValue = targetValue.replace(
        /^([0-9]+[\\.,]?[0-9]{0,2})?.*$/,
        '$1',
      );

      const amount = parseFloat(newRawValue) || 0;

      if (max && amount > max) {
        return;
      }

      setRawValue(newRawValue);

      onChange?.(amount);
    },
    [onChange, max],
  );

  const resultValue = isFocused ? rawValue : formattedValue;

  return (
    <input
      ref={ref}
      className={styles.input}
      value={resultValue}
      onChange={handleOnChange}
      onFocus={handleOnFocus}
      onBlur={handleOnBlur}
      size={resultValue.length || 1}
    />
  );
});

export default AmountInput;
