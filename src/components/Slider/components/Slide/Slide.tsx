import React from 'react';
import { SlidePropType } from './Slide.types';
import styles from './Slide.module.css';

export default function Slide({ children }: SlidePropType) {
  return (
    <div className={styles.slide}>
      {children}
    </div>
  );
}
