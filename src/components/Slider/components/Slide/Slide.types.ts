import { ReactNode } from 'react';

export type SlidePropType = {
  children: ReactNode
};
