import { ReactElement } from 'react';
import { SlidePropType } from './components/Slide';

export type SliderPropType = {
  children: ReactElement<SlidePropType>[];
  onChange?(slideIndex: number): void;
  onSlideShow?(slideIndex: number): void;
};
