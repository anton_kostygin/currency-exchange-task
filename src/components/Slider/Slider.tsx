import React, { useCallback } from 'react';
import ReactSlick from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { SliderPropType } from './Slider.types';
import styles from './Slider.module.css';

export default function Slider({ children, onChange, onSlideShow }: SliderPropType) {
  const onChangeSlide = useCallback((previous: number, next: number) => {
    onChange?.(next);
  }, [onChange]);

  return (
    <div className={styles.container}>
      <ReactSlick dots beforeChange={onChangeSlide} afterChange={onSlideShow}>
        {children}
      </ReactSlick>
    </div>
  );
}
