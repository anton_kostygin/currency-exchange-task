import { AccountType } from 'types/account';

export type ExchangeInfoPropsType = {
  accountFrom: AccountType;
  accountTo: AccountType;
  onActivate(): void;
  active: boolean;
};
