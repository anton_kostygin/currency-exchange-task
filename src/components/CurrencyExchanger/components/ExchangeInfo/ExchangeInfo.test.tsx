import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { QueryClient, QueryClientProvider } from 'react-query';
import useExchangeRates from 'hooks/useExchangeRates';
import { AccountType } from 'types/account';
import { CurrencyExchangerContext } from '../../CurrencyExchanger.context';
import ExchangeInfo from './ExchangeInfo';
import styles from './ExchangeInfo.module.css';

jest.mock('hooks/useExchangeRates');

const queryClient = new QueryClient();
const useExchangeRatesMock = useExchangeRates as jest.MockedFunction<typeof useExchangeRates>;
const setExchangeAmountMock = jest.fn();
const onActivateMock = jest.fn();
const getExchangeRateMock = jest.fn();
const wrapper = ({ children }: { children?: any }) => (
  <QueryClientProvider client={queryClient}>
    <CurrencyExchangerContext.Provider value={{
      exchangeAmount: 100,
      setExchangeAmount: setExchangeAmountMock,
    }}
    >
      {children}
    </CurrencyExchangerContext.Provider>

  </QueryClientProvider>
);

const USD_ACCOUNT: AccountType = {
  currencyCode: 'USD',
  amount: 100,
  currencyName: '',
  isExchangeable: true,
};

const EUR_ACCOUNT: AccountType = {
  currencyCode: 'EUR',
  amount: 200,
  currencyName: '',
  isExchangeable: true,
};

describe('ExchangeInfo', () => {
  beforeEach(() => {
    getExchangeRateMock.mockReturnValue(1.1);
    useExchangeRatesMock.mockReturnValue({
      isLoading: false,
      rates: [],
      getExchangeRate: getExchangeRateMock,
    });
  });

  afterEach(() => {
    queryClient.clear();
    jest.resetAllMocks();
  });

  it('render', async () => {
    const {
      getByText,
      container: { firstChild },
    } = render(<ExchangeInfo
      active={false}
      onActivate={onActivateMock}
      accountTo={USD_ACCOUNT}
      accountFrom={EUR_ACCOUNT}
    />, { wrapper });

    expect(firstChild).toHaveClass(styles.container);
    expect(getByText(EUR_ACCOUNT.currencyCode)).toBeInTheDocument();
    expect(getByText(EUR_ACCOUNT.currencyCode)).toHaveClass(styles.bigText);
    expect(getByText(/€1.00/)).toBeInTheDocument();
    expect(getByText(/€1.00/)).toHaveClass(styles.smallText);
    expect(getByText(/\$1.10/)).toBeInTheDocument();
    expect(getByText(/\$1.10/)).toHaveClass(styles.smallText);
    expect(getByText(/€200.0/)).toBeInTheDocument();
    expect(getByText(/€200.0/)).toHaveClass(styles.smallText);
  });

  it('activate account', async () => {
    const {
      getByDisplayValue,
    } = render(<ExchangeInfo
      active={false}
      onActivate={onActivateMock}
      accountTo={USD_ACCOUNT}
      accountFrom={EUR_ACCOUNT}
    />, { wrapper });

    fireEvent.focus(getByDisplayValue('110.00'));

    expect(onActivateMock).toBeCalledTimes(1);
  });

  it('active account', async () => {
    const {
      getByDisplayValue,
      rerender,
    } = render(<ExchangeInfo
      active={false}
      onActivate={onActivateMock}
      accountTo={USD_ACCOUNT}
      accountFrom={EUR_ACCOUNT}
    />, { wrapper });

    const convertedUsdValue = getByDisplayValue('110.00');

    expect(convertedUsdValue).toBeInTheDocument();

    rerender(<ExchangeInfo
      active
      onActivate={onActivateMock}
      accountTo={USD_ACCOUNT}
      accountFrom={EUR_ACCOUNT}
    />);

    const activeUsdValue = getByDisplayValue('100');

    expect(activeUsdValue).toBeInTheDocument();

    fireEvent.change(activeUsdValue, {
      target: { value: 200 },
    });

    expect(setExchangeAmountMock).toBeCalledWith(200);
  });

  it("can't exchange the same currencies", async () => {
    getExchangeRateMock.mockReturnValue(null);
    const {
      getByText,
      getByDisplayValue,
    } = render(<ExchangeInfo
      active={false}
      onActivate={onActivateMock}
      accountTo={EUR_ACCOUNT}
      accountFrom={EUR_ACCOUNT}
    />, { wrapper });

    expect(() => getByDisplayValue('110.00')).toThrow();

    const sameCurrenciesText = getByText('Same currencies');
    expect(sameCurrenciesText).toBeInTheDocument();
    expect(sameCurrenciesText).toHaveClass(styles.bigText);

    const peleaseSelectAnotherText = getByText('Please select another currency');
    expect(peleaseSelectAnotherText).toBeInTheDocument();
    expect(peleaseSelectAnotherText).toHaveClass(styles.smallText);
  });
});
