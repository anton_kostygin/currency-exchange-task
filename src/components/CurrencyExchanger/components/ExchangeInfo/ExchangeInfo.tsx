import React, {
  useContext, useEffect, useMemo, useRef,
} from 'react';
import classNames from 'classnames';
import { convertAmount, formatCurrency } from 'utils/currency';
import AmountInput from 'components/AmountInput';
import useExchangeRates from 'hooks/useExchangeRates';
import { CurrencyExchangerContext } from '../../CurrencyExchanger.context';
import { ExchangeInfoPropsType } from './ExchangeInfo.types';
import styles from './ExchangeInfo.module.css';

export default function ExchangeInfo({
  accountFrom,
  accountTo,
  onActivate,
  active,
}: ExchangeInfoPropsType) {
  const { getExchangeRate, isLoading } = useExchangeRates();
  const inputRef = useRef<HTMLInputElement>(null);
  const {
    exchangeAmount, setExchangeAmount,
  } = useContext(CurrencyExchangerContext);
  const rate = useMemo(() => getExchangeRate(accountFrom.currencyCode, accountTo.currencyCode),
    [accountFrom.currencyCode, accountTo.currencyCode, getExchangeRate]);
  const exchangeAmountValue = useMemo(() => (active
    ? exchangeAmount
    : convertAmount(exchangeAmount,
      getExchangeRate(accountTo.currencyCode, accountFrom.currencyCode) || 0)
  ),
  [accountFrom.currencyCode, accountTo.currencyCode, active, exchangeAmount, getExchangeRate]);

  useEffect(() => {
    if (!isLoading && active && !inputRef.current?.closest('[aria-hidden=true]')) {
      inputRef.current?.focus();
    }
  }, [active, isLoading]);

  return (
    <div className={styles.container}>
      <div className={styles.col}>
        <p className={styles.bigText}>
          {accountFrom.currencyCode}
        </p>
        <p className={styles.smallText}>
          You have
          {' '}
          {formatCurrency(accountFrom.amount, accountFrom.currencyCode)}
        </p>
      </div>
      <div className={classNames(styles.col, styles.rightCol)}>
        {/* eslint-disable-next-line no-nested-ternary */}
        {isLoading
          ? null
          // eslint-disable-next-line no-nested-ternary
          : rate
            ? (
              <>
                <div className={styles.inputContainer}>
                  {
                !!exchangeAmount && (
                  <p className={styles.bigText}>
                    {active ? '-' : '+'}
                  </p>
                )
              }
                  <AmountInput
                    ref={inputRef}
                    value={exchangeAmountValue}
                    onChange={setExchangeAmount}
                    onFocus={() => {
                      if (!active) {
                        onActivate();
                      }
                    }}
                    max={accountFrom.amount}
                  />
                </div>
                <p className={styles.smallText}>
                  {formatCurrency(1, accountFrom.currencyCode)}
                  {' = '}
                  {formatCurrency(rate, accountTo.currencyCode)}
                </p>
              </>
            )
            : accountTo?.currencyCode === accountFrom?.currencyCode
              ? (
                <div>
                  <p className={styles.bigText}>Same currencies</p>
                  <p className={styles.smallText}>Please select another currency</p>
                </div>
              )
              : <p className={styles.bigText}>Can&apos;t convert</p>}
      </div>
    </div>
  );
}
