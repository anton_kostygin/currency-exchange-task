import React, { useCallback, useContext, useMemo } from 'react';
import useAccounts from 'hooks/useAccounts';
import useExchangeRates from 'hooks/useExchangeRates';
import { convertAmount } from 'utils/currency';
import { CurrencyExchangerContext } from '../../CurrencyExchanger.context';
import { ExchangeButtonPropsType } from './ExchangeButton.types';
import styles from './ExchangeButton.module.css';

export default function ExchangeButton({ accountFrom, accountTo }: ExchangeButtonPropsType) {
  const { getExchangeRate } = useExchangeRates();
  const { updateAccounts } = useAccounts();
  const {
    exchangeAmount, setExchangeAmount,
  } = useContext(CurrencyExchangerContext);
  const rate = useMemo(() => {
    if (!accountFrom || !accountTo) {
      return;
    }

    // eslint-disable-next-line consistent-return
    return getExchangeRate(accountFrom?.currencyCode, accountTo.currencyCode);
  }, [accountFrom, accountTo, getExchangeRate]);

  const onExchange = useCallback(() => {
    if (!accountFrom || !accountTo || !rate) {
      return;
    }

    updateAccounts(
      { currencyCode: accountFrom.currencyCode, amount: exchangeAmount },
      { currencyCode: accountTo.currencyCode, amount: convertAmount(exchangeAmount, rate) },
    );
    setExchangeAmount(0);
  }, [accountFrom, accountTo, exchangeAmount, rate, setExchangeAmount, updateAccounts]);

  const disabled = !rate
    || !accountFrom
    || !accountTo
    || accountFrom.currencyCode === accountTo.currencyCode;

  return (
    <button
      type="button"
      className={styles.exchangeButton}
      disabled={disabled}
      onClick={onExchange}
    >
      Exchange
    </button>
  );
}
