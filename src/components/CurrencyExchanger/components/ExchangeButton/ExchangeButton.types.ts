import { AccountType } from 'types/account';

export type ExchangeButtonPropsType = {
  accountFrom?: AccountType;
  accountTo?: AccountType;
};
