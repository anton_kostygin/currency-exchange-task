import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { QueryClient, QueryClientProvider } from 'react-query';
import useAccounts from 'hooks/useAccounts';
import useExchangeRates from 'hooks/useExchangeRates';
import { AccountType } from 'types/account';
import { CurrencyExchangerContext } from '../../CurrencyExchanger.context';
import ExchangeButton from './ExchangeButton';
import styles from './ExchangeButton.module.css';

jest.mock('hooks/useAccounts');
jest.mock('hooks/useExchangeRates');

const queryClient = new QueryClient();
const useAccountsMock = useAccounts as jest.MockedFunction<typeof useAccounts>;
const useExchangeRatesMock = useExchangeRates as jest.MockedFunction<typeof useExchangeRates>;
const updateAccountsMock = jest.fn();
const wrapper = ({ children }: { children?: any }) => (
  <QueryClientProvider client={queryClient}>
    <CurrencyExchangerContext.Provider value={{
      exchangeAmount: 100,
      setExchangeAmount: jest.fn(),
    }}
    >
      {children}
    </CurrencyExchangerContext.Provider>

  </QueryClientProvider>
);

const USD_ACCOUNT: AccountType = {
  currencyCode: 'USD',
  amount: 100,
  currencyName: '',
  isExchangeable: true,
};

const EUR_ACCOUNT: AccountType = {
  currencyCode: 'EUR',
  amount: 200,
  currencyName: '',
  isExchangeable: true,
};

describe('ExchangeButton', () => {
  beforeEach(() => {
    useAccountsMock.mockReturnValue({
      isLoading: false,
      accounts: [USD_ACCOUNT, EUR_ACCOUNT],
      updateAccounts: updateAccountsMock,
    });
    useExchangeRatesMock.mockReturnValue({
      isLoading: false,
      rates: [],
      getExchangeRate: () => 1.1,
    });
  });

  afterEach(() => {
    queryClient.clear();
    jest.resetAllMocks();
  });

  it('render', async () => {
    const {
      getByText,
    } = render(<ExchangeButton />, { wrapper });

    const exchangeButton = getByText('Exchange');

    expect(exchangeButton).toBeInTheDocument();
    expect(exchangeButton).toHaveClass(styles.exchangeButton);
  });

  it('disabled - not enough info for exchange', async () => {
    const {
      getByText,
      rerender,
    } = render(<ExchangeButton />, { wrapper });

    const exchangeButton = getByText('Exchange');

    expect(exchangeButton).toBeDisabled();

    rerender(<ExchangeButton accountFrom={USD_ACCOUNT} />);

    expect(exchangeButton).toBeDisabled();

    rerender(<ExchangeButton accountTo={USD_ACCOUNT} />);

    expect(exchangeButton).toBeDisabled();

    rerender(<ExchangeButton accountFrom={USD_ACCOUNT} accountTo={USD_ACCOUNT} />);

    expect(exchangeButton).toBeDisabled();

    rerender(<ExchangeButton accountFrom={USD_ACCOUNT} accountTo={EUR_ACCOUNT} />);

    expect(exchangeButton).not.toBeDisabled();
  });

  it('exchange - successful', async () => {
    const {
      getByText,
    } = render(<ExchangeButton accountFrom={USD_ACCOUNT} accountTo={EUR_ACCOUNT} />, { wrapper });

    const exchangeButton = getByText('Exchange');

    fireEvent.click(exchangeButton);

    expect(updateAccountsMock).toBeCalledWith({ amount: 100, currencyCode: 'USD' }, { amount: 110, currencyCode: 'EUR' });
  });
});
