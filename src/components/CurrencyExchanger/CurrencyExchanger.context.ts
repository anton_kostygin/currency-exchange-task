import { createContext } from 'react';

// eslint-disable-next-line import/prefer-default-export
export const CurrencyExchangerContext = createContext<{
  exchangeAmount: number;
  setExchangeAmount(amount: number): void;
}>({
      exchangeAmount: 0,
      setExchangeAmount() {},
    });
