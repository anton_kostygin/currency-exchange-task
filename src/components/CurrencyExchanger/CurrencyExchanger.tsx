import React, { useCallback, useMemo, useState } from 'react';
import Slider, { Slide } from 'components/Slider';
import useAccounts from 'hooks/useAccounts';
import { AccountType } from 'types/account';
import ExchangeButton from './components/ExchangeButton';
import ExchangeInfo from './components/ExchangeInfo';
import { CurrencyExchangerContext } from './CurrencyExchanger.context';
import { ActiveSliderAccount, ExchangeSlider } from './CurrencyExchanger.types';
import styles from './CurrencyExchanger.module.css';

export default function CurrencyExchanger() {
  const { accounts } = useAccounts();
  const reversedAccounts = [...accounts].reverse();
  const [exchangeAmount, setExchangeAmount] = useState(0);
  const [activeSliderAccount, setActiveSliderAccount] = useState<ActiveSliderAccount | undefined>({
    type: ExchangeSlider.FIRST,
    account: accounts[0],
  });
  const [firstSliderAccount, setFirstSliderAccount] = useState(accounts[0]);
  const [secondSliderAccount, setSecondSliderAccount] = useState(reversedAccounts[0]);

  const onFirstSliderChange = useCallback((index: number) => {
    setFirstSliderAccount(accounts[index]);
    if (activeSliderAccount?.type === ExchangeSlider.FIRST) {
      setExchangeAmount(0);
    }
  }, [accounts, activeSliderAccount]);
  const onSecondSliderChange = useCallback((index: number) => {
    setSecondSliderAccount(reversedAccounts[index]);
    if (activeSliderAccount?.type === ExchangeSlider.SECOND) {
      setExchangeAmount(0);
    }
  }, [activeSliderAccount?.type, reversedAccounts]);

  const onSliderActivate = useCallback((type: ExchangeSlider, account: AccountType) => {
    setActiveSliderAccount({ type, account });
    setExchangeAmount(0);
  }, []);

  const onSlideShow = useCallback((type: ExchangeSlider, account: AccountType) => {
    if (activeSliderAccount?.type === type) {
      onSliderActivate(type, account);
    }
  }, [activeSliderAccount?.type, onSliderActivate]);

  const getIsActive = useCallback((
    type: ExchangeSlider,
    account: AccountType,
  ) => activeSliderAccount?.type === type
      && activeSliderAccount?.account === account,
  [activeSliderAccount]);

  const contextValue = useMemo(() => ({
    exchangeAmount,
    setExchangeAmount,
  }), [exchangeAmount]);

  return (
    <CurrencyExchangerContext.Provider value={contextValue}>
      <div className={styles.container}>
        <Slider
          onChange={onFirstSliderChange}
          onSlideShow={(slideIndex) => onSlideShow(ExchangeSlider.FIRST, accounts[slideIndex])}
        >
          {
            accounts.map((account) => (
              <Slide key={account.currencyCode}>
                <ExchangeInfo
                  active={getIsActive(ExchangeSlider.FIRST, account)}
                  accountFrom={account}
                  accountTo={secondSliderAccount}
                  onActivate={() => onSliderActivate(ExchangeSlider.FIRST, account)}
                />
              </Slide>
            ))
          }
        </Slider>
        <Slider
          onChange={onSecondSliderChange}
          onSlideShow={(slideIndex) => onSlideShow(
            ExchangeSlider.SECOND, reversedAccounts[slideIndex],
          )}
        >
          {
            reversedAccounts.map((account) => (
              <Slide key={account.currencyCode}>
                <ExchangeInfo
                  active={getIsActive(ExchangeSlider.SECOND, account)}
                  accountFrom={account}
                  accountTo={firstSliderAccount}
                  onActivate={() => onSliderActivate(ExchangeSlider.SECOND, account)}
                />
              </Slide>
            ))
          }
        </Slider>
        <ExchangeButton
          accountFrom={activeSliderAccount?.account}
          accountTo={activeSliderAccount?.type === ExchangeSlider.FIRST
            ? secondSliderAccount
            : firstSliderAccount}
        />
      </div>

    </CurrencyExchangerContext.Provider>
  );
}
