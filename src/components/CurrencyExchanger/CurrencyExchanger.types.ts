import { AccountType } from '../../types/account';

export enum ExchangeSlider {
  FIRST,
  SECOND,
}

export type ActiveSliderAccount = {
  type: ExchangeSlider,
  account: AccountType;
};
