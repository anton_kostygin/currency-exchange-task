import React from 'react';
import { renderHook } from '@testing-library/react-hooks';
import { QueryClient, QueryClientProvider } from 'react-query';
import fetchData from 'utils/fetchData';
import useAccounts from './useAccounts';

jest.mock('utils/fetchData');

const queryClient = new QueryClient();
const wrapper = ({ children }: { children: any }) => (
  <QueryClientProvider client={queryClient}>
    {children}
  </QueryClientProvider>
);

const mockFetchData = fetchData as jest.MockedFunction<typeof fetchData>;

describe('useAccounts', () => {
  afterEach(() => {
    queryClient.clear();
  });

  it('load accounts', async () => {
    mockFetchData.mockReturnValueOnce(Promise.resolve({ USD: 1000 }));
    mockFetchData.mockReturnValueOnce(Promise.resolve({ USD: 'US Dollar' }));

    const { result, waitFor } = renderHook(() => useAccounts(), { wrapper });
    expect(result.current.isLoading).toBe(true);

    await waitFor(() => !result.current.isLoading);

    expect(result.current.accounts).toEqual([{
      amount: 1000,
      currencyCode: 'USD',
      currencyName: 'US Dollar',
      isExchangeable: true,
    }]);
  });

  it('account with unknown currency', async () => {
    mockFetchData.mockReturnValueOnce(Promise.resolve({ USD: 1000 }));
    mockFetchData.mockReturnValueOnce(Promise.resolve({ EUR: 'Euro' }));

    const { result, waitFor } = renderHook(() => useAccounts(), { wrapper });
    await waitFor(() => !result.current.isLoading);

    expect(result.current.accounts).toEqual([{
      amount: 1000,
      currencyCode: 'USD',
      isExchangeable: false,
    }]);
  });

  it('update accounts', async () => {
    mockFetchData.mockReturnValueOnce(Promise.resolve({ USD: 1000, EUR: 200 }));
    mockFetchData.mockReturnValueOnce(Promise.resolve({ USD: 'US Dollar', EUR: 'Euro' }));

    const { result, waitFor, waitForNextUpdate } = renderHook(() => useAccounts(), { wrapper });
    await waitFor(() => !result.current.isLoading);

    expect(result.current.accounts).toEqual([{
      amount: 1000,
      currencyCode: 'USD',
      currencyName: 'US Dollar',
      isExchangeable: true,
    }, {
      amount: 200,
      currencyCode: 'EUR',
      currencyName: 'Euro',
      isExchangeable: true,
    }]);

    result.current.updateAccounts({ currencyCode: 'USD', amount: 100 }, { currencyCode: 'EUR', amount: 150 });

    await waitForNextUpdate();

    expect(result.current.accounts).toEqual([{
      amount: 900,
      currencyCode: 'USD',
      currencyName: 'US Dollar',
      isExchangeable: true,
    }, {
      amount: 350,
      currencyCode: 'EUR',
      currencyName: 'Euro',
      isExchangeable: true,
    }]);
  });
});
