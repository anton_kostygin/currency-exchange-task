import { GET_EXCHANGE_RATES } from 'constants/api';
import { useCallback } from 'react';
import { useQuery } from 'react-query';
import { ExchangeRate } from 'types/exchangeRate';
import { convertCurrencyRates } from 'utils/currency';
import fetchData from 'utils/fetchData';
import useAccounts from './useAccounts';

export default function useExchangeRates() {
  const { accounts, isLoading: isLoadingAccounts } = useAccounts();
  const fetchRates = useCallback(async () => {
    const currencies = accounts
      .filter((a) => a.isExchangeable)
      .map(({ currencyCode }) => currencyCode) || [];

    const { base, rates } = await fetchData(GET_EXCHANGE_RATES,
      {
        base: 'USD',
        symbols: currencies.join(),
      });

    const usdRate: ExchangeRate = {
      base,
      rates,
    };

    return currencies.map<ExchangeRate>(
      (currencyCode) => convertCurrencyRates(currencyCode, usdRate),
    );
  }, [accounts]);

  const { data: rates, isLoading } = useQuery<ExchangeRate[], unknown, ExchangeRate[]>('exchange_rates', fetchRates, {
    refetchInterval: 10000, // Refresh the rate every 10s
    enabled: !isLoadingAccounts,
  });

  const getExchangeRate = useCallback((from: string, to: string) => rates
    ?.find(({ base }) => base === from)
    ?.rates[to], [rates]);

  return { rates, getExchangeRate, isLoading };
}
