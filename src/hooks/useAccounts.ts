import { GET_ACCOUNTS, GET_CURRENCIES } from 'constants/api';
import { useCallback } from 'react';
import { useQuery, useQueryClient } from 'react-query';
import { AccountType } from 'types/account';
import fetchData from 'utils/fetchData';

export default function useAccounts() {
  const queryClient = useQueryClient();
  const { isLoading, data: accounts } = useQuery<AccountType[], unknown, AccountType[]>('accounts', async () => {
    const data = await Promise.all([
      fetchData(GET_ACCOUNTS),
      fetchData(GET_CURRENCIES),
    ]);
    const [fetchedAccounts, currencies]: [Record<string, number>, Record<string, string>] = data;

    return Object.entries(fetchedAccounts).map<AccountType>(([currencyCode, amount]) => {
      const currencyName = currencies[currencyCode];

      return ({
        amount,
        currencyCode,
        currencyName,
        isExchangeable: !!currencyName,
      });
    });
  }, {
    refetchOnMount: false,
    refetchOnWindowFocus: false,
    refetchInterval: Infinity,
  });

  const updateAccounts = useCallback((
    from: { currencyCode: string, amount: number },
    to: { currencyCode: string, amount: number },
  ) => {
    queryClient.setQueryData('accounts', accounts?.map((account) => {
      if (account.currencyCode === from.currencyCode) {
        return { ...account, amount: account.amount - from.amount };
      }

      if (account.currencyCode === to.currencyCode) {
        return { ...account, amount: account.amount + to.amount };
      }

      return account;
    }));
  }, [accounts, queryClient]);

  return { isLoading, accounts: accounts || [], updateAccounts };
}
