import React from 'react';
import { renderHook } from '@testing-library/react-hooks';
import { QueryClient, QueryClientProvider } from 'react-query';
import fetchData from 'utils/fetchData';
import useAccounts from './useAccounts';
import useExchangeRates from './useExchangeRates';

jest.mock('utils/fetchData');
jest.mock('hooks/useAccounts');

const queryClient = new QueryClient();
const wrapper = ({ children }: { children: any }) => (
  <QueryClientProvider client={queryClient}>
    {children}
  </QueryClientProvider>
);

const mockFetchData = fetchData as jest.MockedFunction<typeof fetchData>;
const mockUseAccounts = useAccounts as jest.MockedFunction<typeof useAccounts>;

describe('useExchangeRates', () => {
  beforeEach(() => {
    mockUseAccounts.mockReturnValue({
      isLoading: false,
      accounts: [
        {
          amount: 100,
          isExchangeable: true,
          currencyName: '',
          currencyCode: 'USD',
        },
        {
          amount: 100,
          isExchangeable: true,
          currencyName: '',
          currencyCode: 'EUR',
        },
        {
          amount: 100,
          isExchangeable: true,
          currencyName: '',
          currencyCode: 'GBP',
        },
      ],
      updateAccounts: jest.fn(),
    });

    mockFetchData.mockReturnValueOnce(Promise.resolve({ base: 'USD', rates: { GBP: 0.5, EUR: 0.8 } }));
  });

  afterEach(() => {
    queryClient.clear();
    jest.resetAllMocks();
  });

  it('load rates', async () => {
    const { result, waitFor } = renderHook(() => useExchangeRates(), { wrapper });
    expect(result.current.isLoading).toBe(true);

    await waitFor(() => !result.current.isLoading);

    expect(result.current.isLoading).toBe(false);

    expect(result.current.rates).toEqual([{
      base: 'USD',
      rates: {
        EUR: 0.8,
        GBP: 0.5,
      },
    },
    {
      base: 'EUR',
      rates: {
        GBP: 0.625,
        USD: 1.25,
      },
    },
    {
      base: 'GBP',
      rates: {
        EUR: 1.6,
        USD: 2,
      },
    }]);
  });

  it('get exchange rate', async () => {
    const { result, waitFor } = renderHook(() => useExchangeRates(), { wrapper });
    await waitFor(() => !result.current.isLoading);

    expect(result.current.getExchangeRate('USD', 'EUR')).toBe(0.8);
    expect(result.current.getExchangeRate('EUR', 'USD')).toBe(1.25);
    expect(result.current.getExchangeRate('USD', 'GBP')).toBe(0.5);
    expect(result.current.getExchangeRate('GBP', 'USD')).toBe(2);
    expect(result.current.getExchangeRate('GBP', 'EUR')).toBe(1.6);
    expect(result.current.getExchangeRate('EUR', 'GBP')).toBe(0.625);
  });
});
